package com.develop.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiciosMensageriaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiciosMensageriaApplication.class, args);
	}

}
