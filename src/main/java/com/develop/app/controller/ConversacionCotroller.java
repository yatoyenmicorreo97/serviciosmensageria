package com.develop.app.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.develop.app.config.validator.MessageValidatorImplements;
import com.develop.app.entity.Mensajes;
import com.develop.app.repository.MensajesI;
import com.develop.app.util.exceptions.ApiUnprocessableEntity;
import com.mongodb.BasicDBObject;

@RestController
@RequestMapping("servicio/conversacion")
@CrossOrigin(origins = "*", methods = {RequestMethod.POST,RequestMethod.GET,RequestMethod.PUT,RequestMethod.DELETE})
public class ConversacionCotroller {

	@Autowired
	private MensajesI conversacionRepository;
	
	@Autowired
	private MessageValidatorImplements validatorImplements;
	
	@GetMapping("/verConversacion")
	public List<com.develop.app.entity.Mensajes> findAll(){
		return conversacionRepository.findAll();
	}
	
	@PostMapping("/crearConversacion")
	public ResponseEntity<?> crearConversacion(@Validated @RequestBody Mensajes mensaje) throws ApiUnprocessableEntity{
		//return conversacionRepository.insert(mensaje);
		this.validatorImplements.validator(mensaje);
		
		mensaje.setIDConversacion(mensaje.getIDEmisor()+"_"+mensaje.getIDReceptor());
		mensaje.setFecha(new Date());
		mensaje.setVisible(true);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(conversacionRepository.insert(mensaje));
	}

	@GetMapping("/getOne")
	public ResponseEntity<?> read(@RequestBody Mensajes mensaje){
		Iterable<Mensajes> iter= conversacionRepository.findByIdConversacion(mensaje.getIDConversacion());
		System.out.println(iter);


		/*if(!iter.){
			return ResponseEntity.notFound().build();
		}*/
		return ResponseEntity.ok(iter.iterator());

	}
	
	@GetMapping("/getOne2/{idConversacion}")
	public ResponseEntity<?> getOneXD(@PathVariable(value = "idConversacion") String idConversacion){
		Iterable<Mensajes> iter= conversacionRepository.findByIdConversacion(idConversacion);

		return ResponseEntity.ok(iter.iterator());
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Mensajes mensajes, @PathVariable(value = "id") String id)throws ApiUnprocessableEntity{
		Optional<Mensajes> opt = conversacionRepository.findById(id);
		this.validatorImplements.validarStatus(mensajes);
		if(!opt.isPresent()){

			return ResponseEntity.notFound().build();

		}

		opt.get().setVisible(mensajes.getVisible());
		return ResponseEntity.status(HttpStatus.CREATED).body(conversacionRepository.save(opt.get()));
	}

	/*@GetMapping("/{idConversacion}")
	public ResponseEntity<?> read(@PathVariable(value = "idConversacion")String idConversacion){
		Optional<Mensajes> opt = @Query(Mensajes = "{'idConversacion' : ?0}")
				List  findAll(String "idConversacion" );
	}*/

}


