package com.develop.app.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Mensajes")
public class Mensajes {

    private static final long serialVersionUID=1L;
	@Id
	private String id;
    private Date fecha;
    private String idConversacion;
    private String idEmisor;
    private String idReceptor;
    private String texto;
    private boolean visible;

    public String getID() { return id; }
    public void setID(String value) { this.id = value; }

    public Date getFecha() { return fecha; }
    public void setFecha(Date value) { this.fecha = value; }

    public String getIDConversacion() { return idConversacion; }
    public void setIDConversacion(String value) { this.idConversacion = value; }

    public String getIDEmisor() { return idEmisor; }
    public void setIDEmisor(String value) { this.idEmisor = value; }

    public String getIDReceptor() { return idReceptor; }
    public void setIDReceptor(String value) { this.idReceptor = value; }

    public String getTexto() { return texto; }
    public void setTexto(String value) { this.texto = value; }

    public boolean getVisible() { return visible; }
    public void setVisible(boolean value) { this.visible = value; }
}
