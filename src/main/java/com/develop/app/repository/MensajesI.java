package com.develop.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.develop.app.entity.Mensajes;
import org.springframework.data.mongodb.repository.Query;
import java.util.Optional;

public interface MensajesI extends MongoRepository<Mensajes, String>{

    @Query("{'idConversacion': ?0}")
    Iterable<Mensajes> findByIdConversacion (String idConversacion);
}
