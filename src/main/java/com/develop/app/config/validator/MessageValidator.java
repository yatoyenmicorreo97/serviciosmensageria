package com.develop.app.config.validator;

import org.springframework.stereotype.Service;

import com.develop.app.entity.Mensajes;
import com.develop.app.util.exceptions.ApiUnprocessableEntity;

//Interface de validacion de los datos recibidos
@Service
public interface MessageValidator {
	void validator(Mensajes request) throws ApiUnprocessableEntity;
	void validarStatus(Mensajes request)throws ApiUnprocessableEntity;
}
