package com.develop.app.config.validator;

import org.springframework.stereotype.Component;

import com.develop.app.entity.Mensajes;
import com.develop.app.util.exceptions.ApiUnprocessableEntity;

@Component
public class MessageValidatorImplements implements MessageValidator{

	@Override
	public void validator(Mensajes request) throws ApiUnprocessableEntity {
		if(request.getIDEmisor() == null || request.getIDEmisor().isEmpty()) {
			this.message("El id del emisor es obligatorio");
		}
		
		if(request.getIDEmisor().length() < 3) {
			this.message("El id del emisor es muy corto, debe tener almenos 3 caracteres");
		}
		
		if(request.getIDReceptor() == null || request.getIDReceptor().isEmpty()) {
			this.message("El id del receptor es obligatorio");
		}
		
		if(request.getIDReceptor().length() < 3) {
			this.message("El id del receptor es muy corto, debe tener almenos 3 caracteres");
		}
		
		if(request.getTexto() == null || request.getTexto().isEmpty()) {
			this.message("El texto del mensaje es obligatorio");
		}
		
		if(request.getTexto().length() < 1) {
			this.message("El texto es muy corto, debe tener almenos 1 caracter");
		}
		if(request.getVisible() == true ){
			this.message("El campo visible solo se puede actualizar a false");
		}

	}

	@Override
	public void validarStatus(Mensajes request) throws ApiUnprocessableEntity {
		if(request.getVisible() == true ){
			this.message("El campo visible solo se puede actualizar a falso");
		}
	}

	private void message(String message) throws ApiUnprocessableEntity {
		throw new ApiUnprocessableEntity(message);
	}

}
